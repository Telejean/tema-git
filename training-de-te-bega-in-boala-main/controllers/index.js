const casaDeDiscuri = require("./casa-de-discuri");
const manea = require("./manea");
const other = require("./other");

const controllers = {
  casaDeDiscuri,
  manea,
  other,
};

module.exports = controllers;
