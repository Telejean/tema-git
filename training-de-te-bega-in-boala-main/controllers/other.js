// Functia sync() => ne ajuta sa creeam tabelele daca acestea NU exista!!!!
// Functia sync({ force: true }) => ne ajuta sa creeam tabelele CHIAR daca INAINTE erau alte TABELE EXISTENTE!!!

const connection = require("../models").connection;

const controller = {
  resetDb: (req, res) => {
    connection
      .sync({ force: true })
      .then(() => {
        res
          .status(201)
          .send({ message: "Baza de date a fost resetata cu succes!" });
      })
      .catch((error) => {
        console.log(error);
        res.status(500).send({
          message: "Resetarea bazei de date a esuat! Verificati consola!",
        });
      });
  },
};

module.exports = controller;
