const express = require("express"); // referinta catre lucrurile exportate de catre modul
// const connection = require("./models").connection;
const router = require("./routes"); // cu ajutorul acestei variabile, avem toate verbele HTTP (post() , put(), delete() etc.) => vezi folderul "routes"
const app = express(); // app-ul il folosim pentru a handle-ui request-urile

app.use(express.json()); // => functia express.json() analizeaza(parseaza) cererile(request-urile) primite ca JSON (se parseaza cu body-parser)

const port = 7070;

app.use("/api", router);


app.use("/*", (req, res) => {
  res.status(200).send("Aplicatia ruleaza, dar aceasta ruta nu este definita!");
});

app.listen(port, () => {
  console.log(`Server-ul ruleaza pe portul ${port}!`);
}); // => listen ne ajuta sa legam conexiunea unui anumit host la port
